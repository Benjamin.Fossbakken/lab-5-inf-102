package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        Map<V, Boolean> visited = new HashMap<>();
        for (V node : graph.getNodes()) {
            visited.put(node, false);
        }

        return dfs(u, v, visited);
    }

    private boolean dfs(V u, V v, Map<V, Boolean> visited) {
        visited.put(u, true);
        for (V next : graph.getNeighbourhood(u)) {
            if (next.equals(v)) {
                return true;
            }
            if (!visited.get(next)) {
                if (dfs(next, v, visited)) {
                    return true;
                }
            }
        }
        return false;
    }

}
